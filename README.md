# TÓM TẮT CÁC DỊCH VỤ CƠ BẢN AWS

# 1. VPC: 

Là phần network của máy ảo EC2.

Region là khu vực, đại khái có khoảng hơn 10 khu vực, riêng US có 3 region, trong 1 region sẽ có nhiều Availability Zone, thường là data center. Region Singapore có 2 Availability Zone (data center) là ap-southeast-1a, ap-southeast-1b. Có thể hiểu như hình:

![Scheme](img/aws_regions.png)


* 1 account có thể tạo nhiều VPC
* 1 VPC tạo nhiều subnet, 1 subnet chỉ nên tạo trong 1 AZ
* 1 VPC sẽ có CIDR là range IP
* Trên CIDR đó sẽ chia ra nhiều subnet

Cho phép tạo 1 hệ thống mạng trong account AWS:

- Các subnet
- Routing table
- Network gateway
- Security setting (cấu hình security group, ACL)

## 1.2 Security group và ACL:

**Sự khác nhau giữa security group và ACL:**

![Scheme](img/securitygroup_vs_acl.PNG)

## 1.3 EIP:

(page 139) là ip có hiệu lực trên internet (public IP), và là ip tĩnh (static IP). Dùng để gán cho EC2 nằm trong VPC. Chú ý:

- Có thể gỡ EIP ra khỏi instance và gán cho Instance khác, cùng VPC hoặc khác VPC, miễn là cùng region.
- EIP của region này không được add cho instance của region khác, dù là cùng VPC.
- EIP gỡ ra khỏi instance thì cũng còn tồn tại trong account, vẫn tính phí.


## 1.4 ELB:

Là loadbalancing, hoạt động như 1 service nằm 
- Có 2 loại: Classic LB và Application LB
- Phân tán traffic cho group of EC2 của 1 hoặc nhiều Availability Zones
- Hỗ trợ HTTP / HTTPS / TCP / SSL
- Phải dùng DNS CNAME, không thể dùng DNS trỏ về IP của ELB. (đã được)
- Hỗ trợ healthcheck, sticky sessions, Connection draining (định timeout cho backend)
- Kết hợp với cloudwatch để làm autoscaling.

# 2. EC2 (limit bandwidth, limit IO, alert threshold)
Phân loại EC2: table trang 96
chú ý khi tạo ec2 xong thì phải vào security group add rule mở port 22, nếu ko thì ko ssh được. (trang 115)

### ec2 vs lightsail
https://stackoverflow.com/questions/40927189/what-is-difference-between-lightsail-and-ec2

# 3. EBS (pay as you go, price)

Đĩa cứng của EC2, dung lượng càng cao tốc độ càng cao. Phân loại:

General Purpose SSD: EBS dưới 1 TB thường khoảng 3k IOPS, 5TB khoảng 15k IOPS, 16TB (maximum) cũng là 15k IOPS. Tốc độ SSD vật lí ngoài thị trường khoảng 80-120k IOPS.

Provisioned IOPS: dạo động từ 4GB - 16TB, có thể setup IOPS ngay khi tạo, max 20k IOPS.

# 4. IAM

# 5. Route53

# 6. S3

Là nơi lưu trữ (mp3,jpg,pdf, file gì cững được). Dịch vụ trong AWS và cả service app bên ngoài AWS đều kết nối đến được.

**S3 gọi là object storage system**, còn dạng lưu trữ file thường thấy như NAS gọi là file storage system. Khác nhau chỗ nào ? mỗi lần upload 1 file lên S3, thì phát sinh 1 object, **1 object gồm 3 phần: metadata, key và nội dung đó**. Hiểu đơn giản như ta có 1 cái chai, nếu bỏ vào NAS thì có đúng 1 cái chai tên là chai bia, nếu bỏ vào S3 thì ta có 3 thứ: `key` có value "cái chai", `metadata`: 1 miếng nhãn dán lên chai có ghi chú thêm vài dòng, ghi gì cũng được, muốn sửa thì đơn giản nhất là nhấn vào metadata, chọn add more metadata, cuối cùng là 1 vật thể hình cái chai bia. **Vậy object là đơn vị nhỏ nhất, chứa 3 thứ: key, metadata, data. Maximum 1 Object là 5TB.**

**bucket** là webfolder, đại khái là folder, chứa nhiều object. Tên bucket tối đa **63 kí tự**, bucket nào cũng có 1 path, ví dụ ta có bucket lele, trong bucket chứa 1 bản nhạc có key là Lac_troi.mp3, bấm vào là download được file này từ internet.

`http://lele.s3.amazonaws.com/Lac_troi.mp3`

**mỗi acc AWS giới hạn tạo 100 bucket**

## Phân loại:
Amazon S3 Standard – Infrequent Access (Standard-IA): loại default, an toàn cao nhất
Amazon S3 Reduced Redundancy Storage (RRS): độ an toàn giảm xuống còn 99.99%
Amazon Glacier: rẻ nhất siêu chậm, giới hạn 40TB, chỉ dùng lưu trữ nhiều năm.

Cụ thể: page 69, AWS-Certified-Solutions-Architect-Official-Study-Guide.pdf






S3	ACLs	allow	you	to	grant	certain	coarse-grained	permissions: READ,WRITE,	or FULL-CONTROL	at	the	object	or	bucket	level
S3	bucket	policy,	you	can	specify	who	can	access	the	bucket
IAM	policies	may	be	associated	directly	with	IAM	principals	that	grant	access	to	an Amazon	S3	bucket
versioning: bucket level, rollback every object
### aws cli (add access key), list vpc, ec2
Với <aws-access-key>/<aws-secret-key> được lấy tại Account Settings > Security Credentials > Access Credentials. Nếu chưa tạo thì bạn Create a new Access Key
Với <region-url> được liệt kê trên shell bằng command ec2-describe-regions